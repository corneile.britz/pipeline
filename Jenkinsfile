pipeline {
  agent any

    triggers {
      pollSCM('* * * * *')
    }

  options {
    skipStagesAfterUnstable()
      parallelsAlwaysFailFast()
  }

  parameters {
    string(name: 'USERNAME', defaultValue: '1.23', description: 'Enter a username')
      string(name: 'REVISION', defaultValue: 'JOHN', description: 'Enter revision')
      password(name: 'PASSWORD', defaultValue: 'DOE', description: 'Enter a password')
  }

  stages {
    stage('Initialise') {
      steps {
        echo "Revision ${params.REVISION}"
          echo "Username ${params.USERNAME}"
          echo "Password: ${params.PASSWORD}"
      }
    }

    stage('Build') {
      steps {
        build job: 'app_build', parameters: [
          string(name: 'REVISION', value: '${params.REVISION}'),
        ]
      }
    }

    stage('Test') {
      stages {

        stage('Functional Testing') {

          parallel {

            stage('Regression test') {
              steps {
                build job: 'app_test', parameters: [
                  string(name: 'ENV', value: 'TST'),
                  string(name: 'TYPE', value: 'Regression')
                ]
              }
            }

            stage('Integration test') {
              steps {
                build job: 'app_test', parameters: [
                  string(name: 'ENV', value: 'INT'),
                  string(name: 'TYPE', value: 'Integration')
                ]
              }
            }

          }

        }

        stage('Non-Functional Testing') {

          parallel {
            stage('Performance test') {
              steps {
                build job: 'app_test', parameters: [
                  string(name: 'ENV', value: 'NFR'),
                  string(name: 'TYPE', value: 'Performance')
                ]
              }
            }

            stage('Security test') {
              steps {
                build job: 'app_test', parameters: [
                  string(name: 'ENV', value: 'NFR'),
                  string(name: 'TYPE', value: 'Security')
                ]
              }
            }

          }

        }

      }
    }

    stage('Quality Assurance') {
      input {
        message "Proceed to QA?"
          submitter "alice,bob"
          parameters {
            string(name: 'QAPERSON', defaultValue: '', description: 'Name')
          }
      }
      steps {
        echo "Approved by ${params.QAPERSON}"

          build job: 'app_deploy', parameters: [
          string(name: 'ENV', value: 'QA'),
            string(name: 'CMD', value: 'CREATE')
          ]
      }
    }

    stage('Release') {
      input {
        message "Proceed to Production?"
          submitter "alice,bob"
          parameters {
            string(name: 'PRDPERSON', defaultValue: '', description: 'Name')
          }
      }
      steps {
        echo "Approved by ${params.PRDPERSON}"

          build job: 'app_deploy', parameters: [
          string(name: 'ENV', value: 'PRD'),
          string(name: 'CMD', value: 'CREATE')
          ]
      }
    }

  }

  post {
    always {
      echo 'I will always say Hello again!'
    }
  }
}
